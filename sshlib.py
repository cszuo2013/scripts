
DEBUG = False
TimeOut = 5
import struct
from select import select

__all__ = [
    'parse_byte', 'generate_byte',
    'parse_uint32', 'generate_uint32',
    'parse_string', 'generate_string',
    'parse_name_list', 'generate_name_list',
    'parse_mpint', 'generate_mpint',
    'random_bytes', 'get_32_byte_repr'
]

def parse_byte(data, start_index):
  return (start_index + 1, struct.unpack('B', data[start_index])[0])

def generate_byte(value):
  return struct.pack('B', value)

def parse_uint32(data, start_index):
  return (start_index + 4, struct.unpack('>I', data[start_index:start_index + 4])[0])

def generate_uint32(value):
  return struct.pack('>I', value)

def parse_string(data, start_index):
  len_size = 4
  strlen = struct.unpack('>I', data[start_index:start_index + len_size])[0]
  start_index += len_size
  string = data[start_index:start_index + strlen]
  start_index += strlen
  return (start_index, string)

def generate_string(string):
  return struct.pack('>I', len(string)) + string

def parse_name_list(data, start_index):
  start_index, name_list = parse_string(data, start_index)

  return (start_index, name_list.split(','))

def generate_name_list(items):
  return generate_string(','.join(items))

def _bitflip_byte(data):
  return struct.pack('B', (~struct.unpack('B', data)[0]) % 0x100)

def _twos_complement(byte_array):
  byte_array = [_bitflip_byte(b) for b in byte_array]

  i = 0
  while byte_array[i] == '\xff':
    byte_array[i] = '\x00'
    i += 1
  byte_array[i] = struct.pack('B', struct.unpack('B', byte_array[i])[0] + 1)

  return byte_array

def parse_mpint(data, start_index):
  start_index, string = parse_string(data, start_index)
  num = 0

  negative = False
  if len(string) > 0:
    if struct.unpack('B', string[0])[0] >> 7 == 1:
      negative = True

  for char in string:
    if negative:
      char = _bitflip_byte(char)
    num = (num << 8) + struct.unpack('B', char)[0]

  if negative:
    num = -(num + 1)

  return (start_index, num)

def generate_mpint(num):
  negative = False
  if num < 0:
    num = -num
    negative = True

  string = []
  if num != 0:
    while num / 256 > 0:
      string.append(struct.pack('B', num % 256))
      num /= 256
    string.append(struct.pack('B', num % 256))

    # If the highest bit *should* be set
    if (num % 256) >> 7 == 1:
      string.append('\x00')

    # If the highest bit is set, add a zero padding byte
    if negative:
      string = _twos_complement(string)

  return generate_string(''.join(reversed(string)))

def random_bytes(num_bytes):
  return open('/dev/urandom').read(num_bytes)

def get_32_byte_repr(num):
  return struct.pack(
      '>QQQQ',
      (num & (0xffffffffffffffff << 192)) >> 192,
      (num & (0xffffffffffffffff << 128)) >> 128,
      (num & (0xffffffffffffffff << 64)) >> 64,
      (num & 0xffffffffffffffff)
  )

#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================

import hashlib
import hmac
import os
import random
import re
import socket

from Crypto.Cipher import AES
from Crypto.Util import Counter
import ecdsa
import ecdsa.util

SSH_PORT = 22
COOKIE_LEN = 16
KEX_RESERVED_BYTES_LEN = 4
AES_BLOCK_LEN = 16
SHA1_LEN = 20
MIN_PADDING_LEN = 4
KEX_ALGORITHM = 'diffie-hellman-group14-sha1'
SERVER_HOST_KEY_ALGORITHM = 'ecdsa-sha2-nistp256'
ENCRYPTION_ALGORITHM = 'aes128-ctr'
MAC_ALGORITHM = 'hmac-sha1'
COMPRESSION_ALGORITHM = 'none'

SSH_MSG_NUMS = {
    'SSH_MSG_DISCONNECT': 1,
    'SSH_MSG_KEXINIT': 20,
    'SSH_MSG_NEWKEYS': 21,
    'SSH_MSG_KEXDH_INIT': 30,
    'SSH_MSG_KEXDH_REPLY': 31,
}

class SSHTransportConnection(object):
  '''An SSH transport connection - allows low-level communication with a server over SSH.

  You almost certainly want an SSHConnection object, not this.

  Args:
    hostname (string): The hostname of the server to communicate with.

  Attributes:
    hostname (string): The hostname of the server to communicate with.
    session_id (string): The ID of the negotiated session, which can be passed to higher level
      services, running on top of the transport layer.
  '''

  def __init__(self, hostname, prot=SSH_PORT):
    self.hostname = hostname
    self.prot = prot
    self.session_id = None

    self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self._encryption_negotiated = False
    self._client_id_string = 'SSH-2.0-karlassh\r\n'
    self._server_id_string = ''

    # Whole session variables
    self._packets_received_counter = 0
    self._packets_sent_counter = 0
    self._aes_client_to_server = None
    self._aes_server_to_client = None
    self._integrity_key_client_to_server = None
    self._integrity_key_server_to_client = None

  def connect(self):
    '''Open a connection to the remote server.'''
    try:
      self._socket.connect((self.hostname, self.prot))
    except Exception,e:
      return e

    self._send_and_receive_id_strings()
    self._do_key_exchange()

  def disconnect(self):
    '''Close the connection to the remote server.'''

    msg = []
    msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_DISCONNECT']))
    msg.append(generate_uint32(11)) # SSH_DISCONNECT_BY_APPLICATION
    msg.append(generate_string('Closed by client'))
    msg.append(generate_string(''))
    self.send(''.join(msg))

    self._socket.close()

    if DEBUG: print 'Disconnected!'

  def close(self):
    try:
      self.disconnect()
    except:
      pass

  def read(self):
    '''Read a packet from the remote server.

    Assuming the initial connection has completed (i.e. #connect has been called, and returned),
    this data will be encrypted, and its authenticity guaranteed.

    Returns (string): the data sent by the remote server.
    '''

    # Read the first <block_len> bytes of the packet, decrypt it if necessary, and parse out the
    # remaining packet length
    #initial_packet = self._socket.recv(AES_BLOCK_LEN)
    initial_packet = select([self._socket], [], [],TimeOut)[0][0].recv(AES_BLOCK_LEN)
    if self._encryption_negotiated:
      initial_packet = self._aes_server_to_client.decrypt(initial_packet)
    _, packet_len = parse_uint32(initial_packet, 0)
    if DEBUG: print 'lenb',packet_len
    if DEBUG: print 'AES_BLOCK_LEN',AES_BLOCK_LEN
    if DEBUG: print 'recv',packet_len - (AES_BLOCK_LEN - 4)


    # Read the remaining bytes of the packet, decrypting if necessary, and checking the MAC
    wantlen = packet_len - (AES_BLOCK_LEN - 4)
    if wantlen != 0:
      remaining_msg = ''
      while wantlen != len(remaining_msg):
        #remaining_msg += self._socket.recv(wantlen - len(remaining_msg))
        remaining_msg += select([self._socket], [], [],TimeOut)[0][0].recv(wantlen - len(remaining_msg))
    else:
      remaining_msg = ''
    if self._encryption_negotiated:
      remaining_msg = self._aes_server_to_client.decrypt(remaining_msg)

      # Read and verify the MAC
      #received_mac = self._socket.recv(SHA1_LEN)
      received_mac = select([self._socket], [], [],TimeOut)[0][0].recv(SHA1_LEN)
      calculated_mac = hmac.new(
          self._integrity_key_server_to_client,
          generate_uint32(self._packets_received_counter) + initial_packet + remaining_msg,
          hashlib.sha1
      ).digest()
      assert received_mac == calculated_mac, \
        'MACs did not match: %s != %s' % (repr(received_mac), repr(calculated_mac))
      if DEBUG: print 'MAC validated correctly!'

    # Pull the payload out of the message
    data = (initial_packet + remaining_msg)[4:]
    index, padding_len = parse_byte(data, 0)
    payload_len = packet_len - padding_len - index
    payload = data[index:payload_len + index]

    self._packets_received_counter += 1
    if DEBUG: print '< Received: %s' % repr(payload)

    return payload

  def send(self, payload):
    '''Send a packet to the remote server.

    Assuming the initial connection has completed (i.e. #connect has been called, and returned),
    this data will be encrypted, and its authenticity guaranteed.

    Args:
      payload (string): The data to send to the remote server.
    '''

    # This maths is horrific, but essentially we're calculating how much padding we need to add,
    # given that we must have at least 4 bytes, and that the total message length must be a multiple
    # of the AES block length
    padding_len = MIN_PADDING_LEN
    padding_len += AES_BLOCK_LEN - ((4 + 1 + len(payload) + padding_len) % AES_BLOCK_LEN)
    packet_len = 1 + len(payload) + padding_len

    msg_parts = []
    msg_parts.append(generate_uint32(packet_len))
    msg_parts.append(generate_byte(padding_len))
    msg_parts.append(payload)
    msg_parts.append(random_bytes(padding_len))

    msg = ''.join(msg_parts)
    # If the packet is encrypted, add the MAC and encrypt the message. The weird order of operations
    # here is because SSH is encrypt-and-mac (that is, the mac is on the plaintext, which is also
    # encrypted), rather than encrypt-then-mac or mac-then-encrypt
    if self._encryption_negotiated:
      mac = hmac.new(
          self._integrity_key_client_to_server,
          generate_uint32(self._packets_sent_counter) + msg,
          hashlib.sha1
      ).digest()

      msg = self._aes_client_to_server.encrypt(msg)
      msg += mac

    self._packets_sent_counter += 1
    if DEBUG: print '> Sending: %s' % repr(''.join(msg))
    self._socket.send(msg)

  def _send_and_receive_id_strings(self):

    # Receive the server's ID string (max size 255, as specified by the RFC)
    #self._server_id_string = self._socket.recv(255)
    self._server_id_string = select([self._socket], [], [],TimeOut)[0][0].recv(255)
    ##match = re.match(r'SSH-([^-]+)-([^ ]+)(?:( .*))?\r\n', self._server_id_string)
    ##assert match, 'Could not parse server ID string ' + self._server_id_string
    
    assert 'SSH-2.' in self._server_id_string, 'Could not parse server ID string ' + self._server_id_string
    
    # Send our own header
    self._socket.sendall(self._client_id_string)

    # Check that we're speaking the right protocol
    ##proto_version, software_version, comments = match.groups()
    ##assert proto_version == '2.0', "Unknown SSH protocol version" % proto_version
    
    if DEBUG: print "Great! I'm speaking to %s (%s)" % (software_version, comments)

  def _do_key_exchange(self):
    # Generate and send our side of the kex exchange handshake
    client_kex_init = _generate_client_kex_init()
    self.send(client_kex_init)

    # Receive the server's side of the key exchange handshake
    server_kex_init = self.read()
    data_ptr, ssh_msg_type = parse_byte(server_kex_init, 0)
    assert ssh_msg_type == SSH_MSG_NUMS['SSH_MSG_KEXINIT']

    # Read the cookie from the server
    cookie = server_kex_init[data_ptr:data_ptr + COOKIE_LEN]
    data_ptr += COOKIE_LEN
    if DEBUG: print 'Cookie: %s' % repr(cookie)

    # Read the algorithm lists from the server
    data_ptr, kex_algorithms = parse_name_list(server_kex_init, data_ptr)
    data_ptr, server_host_key_algorithms = parse_name_list(server_kex_init, data_ptr)
    data_ptr, encryption_algorithms_client_to_server = parse_name_list(server_kex_init, data_ptr)
    data_ptr, encryption_algorithms_server_to_client = parse_name_list(server_kex_init, data_ptr)
    data_ptr, mac_algorithms_client_to_server = parse_name_list(server_kex_init, data_ptr)
    data_ptr, mac_algorithms_server_to_client = parse_name_list(server_kex_init, data_ptr)
    data_ptr, compression_algorithms_client_to_server = parse_name_list(server_kex_init, data_ptr)
    data_ptr, compression_algorithms_server_to_client = parse_name_list(server_kex_init, data_ptr)
    data_ptr, _ = parse_name_list(server_kex_init, data_ptr)
    data_ptr, _ = parse_name_list(server_kex_init, data_ptr)

    # Check that the server did not try to predict the key exchange protocol we'd be using
    data_ptr, first_kex_packet_follows = parse_byte(server_kex_init, data_ptr)
    assert first_kex_packet_follows == 0, 'Additional data in key exchange packet'

    # Check that the reserved bytes are also present in the message
    assert len(server_kex_init) == data_ptr + KEX_RESERVED_BYTES_LEN, \
      'Wrong amount of data left in packet'

    # Check that we'll be able to talk to this server correctly
    assert KEX_ALGORITHM in kex_algorithms
    assert SERVER_HOST_KEY_ALGORITHM in server_host_key_algorithms
    assert ENCRYPTION_ALGORITHM in encryption_algorithms_client_to_server
    assert ENCRYPTION_ALGORITHM in encryption_algorithms_server_to_client
    assert MAC_ALGORITHM in mac_algorithms_client_to_server
    assert MAC_ALGORITHM in mac_algorithms_server_to_client
    assert COMPRESSION_ALGORITHM in compression_algorithms_client_to_server
    assert COMPRESSION_ALGORITHM in compression_algorithms_server_to_client

    # Derive Diffie Hellman shared keys
    self._run_diffie_hellman_group14_sha1_key_exchange(server_kex_init, client_kex_init)

    # Swap to using those keys
    if DEBUG: print 'a'
    self.send(generate_byte(SSH_MSG_NUMS['SSH_MSG_NEWKEYS']))
    if DEBUG: print 'b'
    response = self.read()
    if DEBUG: print 'c'
    index, response_type = parse_byte(response, 0)
    assert response_type == SSH_MSG_NUMS['SSH_MSG_NEWKEYS'], \
      'Unknown SSH message type: %d' % response_type
    assert index == len(response), 'Additional data in response'

    self._encryption_negotiated = True

    if DEBUG: print 'Successfully exchanged keys!'

  def _run_diffie_hellman_group14_sha1_key_exchange(self, server_kex_init, client_kex_init):
    # q, g, and p from https://tools.ietf.org/html/rfc3526#section-3
    q = 2 ** 2048
    g = 2
    p = int('''
      0xFFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E34
      04DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F4
      06B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8
      FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E
      462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2
      261898FA051015728E5A8AACAA68FFFFFFFFFFFFFFFF
    '''.replace(' ', '').replace('\n', ''), 16)

    x = random.SystemRandom().randint(2, q - 1)
    e = pow(g, x, p)

    # Send public key to server
    msg = []
    msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_KEXDH_INIT']))
    msg.append(generate_mpint(e))
    self.send(''.join(msg))

    # Receive (K_S || f || s) from the server
    # i.e. host key blob, f, and the signature, from the server
    msg = self.read()
    index, ssh_msg_type = parse_byte(msg, 0)
    assert ssh_msg_type == SSH_MSG_NUMS['SSH_MSG_KEXDH_REPLY']

    index, host_key_blob = parse_string(msg, index)
    index, f = parse_mpint(msg, index)
    index, signature = parse_string(msg, index)

    # Calculate a verifying key from the host key blob
    verifying_key = self._get_verifying_key(host_key_blob)

    # Also calculate the shared key, exchange hash, and session ID (this is the same as the exchange
    # hash)
    shared_key = pow(f, x, p)
    hashed_data = \
      generate_string(self._client_id_string.strip('\r\n')) + \
      generate_string(self._server_id_string.strip('\r\n')) + \
      generate_string(client_kex_init) + \
      generate_string(server_kex_init) + \
      generate_string(host_key_blob) + \
      generate_mpint(e) + \
      generate_mpint(f) + \
      generate_mpint(shared_key)
    exchange_hash = hashlib.sha1(hashed_data).digest()
    self.session_id = exchange_hash

    # Pull out the signature blob from the message
    index, ecdsa_identifier = parse_string(signature, 0)
    assert ecdsa_identifier == SERVER_HOST_KEY_ALGORITHM, \
      'Unknown signature type: %s' % ecdsa_identifier
    index, signature_blob = parse_string(signature, index)

    index, r = parse_mpint(signature_blob, 0)
    index, s = parse_mpint(signature_blob, index)

    # Verify that the signature on the message is correct
    assert verifying_key.verify(
        get_32_byte_repr(r) + get_32_byte_repr(s),
        exchange_hash,
        hashfunc=hashlib.sha256,
        sigdecode=ecdsa.util.sigdecode_string
    )
    if DEBUG: print 'Signature validated correctly! OMG!'

    # Derive *all* the keys!
    key_derivation_options = {
        'shared_key': shared_key,
        'exchange_hash': exchange_hash,
        'session_id': self.session_id,
    }

    # Client to server keys (these hard-coded ASCII letters brought to you by the RFC's key
    # derivation function: https://tools.ietf.org/html/rfc4253#section-7.2)
    initial_iv_client_to_server = _derive_encryption_key(
        key_derivation_options, 'A', AES_BLOCK_LEN)
    ctr = Counter.new(
        AES_BLOCK_LEN * 8,
        initial_value=int(initial_iv_client_to_server.encode('hex'), AES_BLOCK_LEN))
    encryption_key_client_to_server = _derive_encryption_key(
        key_derivation_options, 'C', AES_BLOCK_LEN)
    self._aes_client_to_server = AES.new(encryption_key_client_to_server, AES.MODE_CTR, counter=ctr)
    self._integrity_key_client_to_server = _derive_encryption_key(key_derivation_options, 'E')

    # Server to client keys
    initial_iv_server_to_client = _derive_encryption_key(
        key_derivation_options, 'B', AES_BLOCK_LEN)
    ctr = Counter.new(
        AES_BLOCK_LEN * 8,
        initial_value=int(initial_iv_server_to_client.encode('hex'), AES_BLOCK_LEN))
    encryption_key_server_to_client = _derive_encryption_key(
        key_derivation_options, 'D', AES_BLOCK_LEN)
    self._aes_server_to_client = AES.new(encryption_key_server_to_client, AES.MODE_CTR, counter=ctr)
    self._integrity_key_server_to_client = _derive_encryption_key(key_derivation_options, 'F')

  def _get_verifying_key(self, host_key_blob):
    # Parse the received data from the host_key_blob
    index, host_key_type = parse_string(host_key_blob, 0)
    index, curve_name = parse_string(host_key_blob, index)
    index, host_public_key = parse_string(host_key_blob, index)

    # Find the expected host key in ~/.ssh/known_hosts
    expected_host_key_type = None
    expected_host_public_key = None
    known_hosts_filename = os.path.expanduser('~/.ssh/known_hosts')
    for line in open(known_hosts_filename, 'r'):
      if len(line.strip()) > 0:
        current_hostname, current_key_type, current_key = line.split(' ')
        if current_hostname == self.hostname:
          expected_host_key_type = current_key_type
          expected_host_public_key = current_key.decode('base64')
          break

    # If we *did* find the host key (i.e. we've already connected to this server), check that
    # everything matches
    if expected_host_key_type is not None:
      assert host_key_type == expected_host_key_type, 'Unexpected host key type: %s' % host_key_type
      assert curve_name == 'nistp256', 'Unknown curve name: %s' % curve_name
      assert host_key_blob == expected_host_public_key, \
        'Unexpected host public key: %s' % repr(host_key_blob)

    # Otherwise, if we haven't seen the host key before, prompt the user to see if they're okay with
    # that
    else:
      assert host_key_type == 'ecdsa-sha2-nistp256', 'Unknown host key type: %s' % host_key_type
      key_fingerprint = hashlib.sha256(host_key_blob).digest().encode('base64')
      # Remove the base64-added new lines, and the padding '=' characters
      key_fingerprint = key_fingerprint.replace('\n', '').rstrip('=')

      if DEBUG: print "The authenticity of host '%s' can't be established." % self.hostname
      if DEBUG: print "ECDSA key fingerprint is SHA256:%s." % key_fingerprint
      #answer = raw_input("Are you sure you want to continue connecting (yes/no)?\n").strip()
      #while answer not in ['yes', 'no', '']:
      #  answer = raw_input("Please type 'yes' or 'no': ").strip()

      # Add key to ~/.ssh/known_hosts
      #answer = 'yes'
      #if answer == 'yes':
      #  with open(known_hosts_filename, 'a') as known_hosts_file:
      #    host_key_base64 = host_key_blob.encode('base64').replace('\n', '')
      #    known_hosts_file.write('%s %s %s\n' % (self.hostname, host_key_type, host_key_base64))

      #else:
      #  assert False, 'Host key verification failed.'

    # NFI why we need to skip a byte here - I can't find this format documented anywhere. I assume
    # this is some kind of type indicator.
    assert host_public_key[0] == '\x04'
    return ecdsa.VerifyingKey.from_string(host_public_key[1:], curve=ecdsa.NIST256p)

def _derive_encryption_key(opts, id_char, key_length=20):
  assert key_length <= SHA1_LEN

  return hashlib.sha1(
      generate_mpint(opts['shared_key']) + \
      opts['exchange_hash'] + \
      id_char + \
      opts['session_id']
  ).digest()[:key_length]

def _generate_client_kex_init():
  msg = []
  msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_KEXINIT']))
  msg.append(random_bytes(COOKIE_LEN))

  msg.append(generate_name_list([KEX_ALGORITHM]))
  msg.append(generate_name_list([SERVER_HOST_KEY_ALGORITHM]))
  msg.append(generate_name_list([ENCRYPTION_ALGORITHM]))
  msg.append(generate_name_list([ENCRYPTION_ALGORITHM]))
  msg.append(generate_name_list([MAC_ALGORITHM]))
  msg.append(generate_name_list([MAC_ALGORITHM]))
  msg.append(generate_name_list([COMPRESSION_ALGORITHM]))
  msg.append(generate_name_list([COMPRESSION_ALGORITHM]))
  msg.append(generate_name_list([]))
  msg.append(generate_name_list([]))

  msg.append(generate_byte(0)) # Additional data being sent = False
  msg.append('\x00\x00\x00\x00') # Reserved bytes

  return ''.join(msg)
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================

from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA

SSH_MSG_NUMS = {
    'SSH_MSG_DISCONNECT': 1,
    'SSH_MSG_KEXINIT': 20,
    'SSH_MSG_NEWKEYS': 21,
    'SSH_MSG_KEXDH_INIT': 30,
    'SSH_MSG_KEXDH_REPLY': 31,
    'SSH_MSG_SERVICE_REQUEST': 5,
    'SSH_MSG_SERVICE_ACCEPT': 6,
    'SSH_MSG_USERAUTH_REQUEST': 50,
    'SSH_MSG_USERAUTH_FAILURE': 51,
    'SSH_MSG_USERAUTH_SUCCESS': 52,
    'SSH_MSG_GLOBAL_REQUEST': 80,
    'SSH_MSG_REQUEST_FAILURE': 82,
    'SSH_MSG_CHANNEL_OPEN': 90,
    'SSH_MSG_CHANNEL_OPEN_CONFIRMATION': 91,
    'SSH_MSG_CHANNEL_WINDOW_ADJUST': 93,
    'SSH_MSG_CHANNEL_DATA': 94,
    'SSH_MSG_CHANNEL_CLOSE': 97,
    'SSH_MSG_CHANNEL_REQUEST': 98,
    'SSH_MSG_CHANNEL_SUCCESS': 99,
}

NUM_2_MSG = {}
for key in SSH_MSG_NUMS.keys():
  NUM_2_MSG[SSH_MSG_NUMS[key]]=key

SSH_USERAUTH_STRING = 'ssh-userauth'


class SSHConnection(object):
  '''An SSH connection - allows communication with a remote server over SSH.

  Args:
    hostname (string): The hostname of the server to communicate with.
    username (string): The username to be used for authentication.
    keyfile (string): The filename of the private key that will be used for authentication.

  Attributes:
    hostname (string): The hostname of the server to communicate with.
    username (string): The username of the server to communicate with.
    keyfile (string): The filename of the private key that will be used for authentication.
  '''

  def __init__(self, hostname, username, keyfile):
    self.username = username
    self.keyfile = keyfile

    self._ssh_transport_connection = SSHTransportConnection(hostname)

    # ssh-connection variables
    self._local_channel_number = 0
    self._remote_channel_number = None

  def connect(self):
    '''Open an authenticated connection to the remote server.'''

    self._ssh_transport_connection.connect()
    self._do_user_auth()
    self._create_ssh_connection()

  def disconnect(self):
    '''Cleanly close the connection to the remote server.'''

    # Send our exit status
    msg = []
    msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_CHANNEL_REQUEST']))
    msg.append(generate_uint32(self._remote_channel_number))
    msg.append(generate_string('exit-status'))
    msg.append(generate_byte(0)) # False
    msg.append(generate_uint32(0)) # Exit status = 0
    self._ssh_transport_connection.send(''.join(msg))

    # Then close the channel
    msg = []
    msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_CHANNEL_CLOSE']))
    msg.append(generate_uint32(self._remote_channel_number))
    self._ssh_transport_connection.send(''.join(msg))

    # Read back the remote side's exit status
    data = self._ssh_transport_connection.read()
    index, msg_type = parse_byte(data, 0)
    index, recipient_channel = parse_uint32(data, index)
    index, request_type = parse_string(data, index)
    index, want_reply_byte = parse_byte(data, index)
    want_reply = want_reply_byte != 0
    index, exit_status = parse_uint32(data, index)

    assert msg_type == SSH_MSG_NUMS['SSH_MSG_CHANNEL_REQUEST']
    assert recipient_channel == self._local_channel_number
    assert request_type == 'exit-status'
    assert not want_reply

    # Disconnect at the transport layer
    self._ssh_transport_connection.disconnect()

    return exit_status

  def read(self):
    '''Read data from the remote server.

    This data will be encrypted, and its authenticity guaranteed (both client-to-server and
    server-to-client).

    Returns (string): the data sent by the remote server.
    '''

    data = self._ssh_transport_connection.read()
    index, msg_type = parse_byte(data, 0)
    index, recipient_channel = parse_uint32(data, index)
    index, channel_data = parse_string(data, index)

    assert msg_type == SSH_MSG_NUMS['SSH_MSG_CHANNEL_DATA']
    assert recipient_channel == self._local_channel_number

    return channel_data

  def send(self, payload):
    '''Send data to the remote server.

    This data will be encrypted, and its authenticity guaranteed (both client-to-server and
    server-to-client).

    Args:
      payload (string): the data to be sent to the remote server.
    '''

    msg = []
    msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_CHANNEL_DATA']))
    msg.append(generate_uint32(self._remote_channel_number))
    msg.append(generate_string(payload))
    self._ssh_transport_connection.send(''.join(msg))

  def _do_user_auth(self):
    # Ask the server whether it supports doing SSH user auth
    msg = []
    msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_SERVICE_REQUEST']))
    msg.append(generate_string(SSH_USERAUTH_STRING))
    self._ssh_transport_connection.send(''.join(msg))

    # Check that it says yes
    data = self._ssh_transport_connection.read()
    index, msg_type = parse_byte(data, 0)
    assert msg_type == SSH_MSG_NUMS['SSH_MSG_SERVICE_ACCEPT'], \
      'Unknown message type received: %d' % msg_type
    index, service_name = parse_string(data, index)
    assert service_name == SSH_USERAUTH_STRING

    if DEBUG: print "Let's do ssh-userauth!"

    # Ask the server which authentication methods it supports
    msg = []
    msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_USERAUTH_REQUEST']))
    msg.append(generate_string(self.username.encode('utf-8')))
    msg.append(generate_string('ssh-connection'))
    msg.append(generate_string('none'))
    self._ssh_transport_connection.send(''.join(msg))

    # Check that publickey is one of them
    data = self._ssh_transport_connection.read()
    index, msg_type = parse_byte(data, 0)
    index, supported_auth_methods = parse_name_list(data, index)
    index, partial_success_byte = parse_byte(data, index)
    partial_success = partial_success_byte != 0
    if DEBUG: print 'supported authentication functions', supported_auth_methods
    assert msg_type == SSH_MSG_NUMS['SSH_MSG_USERAUTH_FAILURE'], \
      'Unknown message type: %d' % msg_type
    assert 'password' in supported_auth_methods, \
      'Server does not support password authentication'
    assert not partial_success
    '''
    # Try to public key auth
    rsa_key = RSA.importKey(open(self.keyfile))
    pkcs_key = PKCS1_v1_5.new(rsa_key)

    msg = []
    msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_USERAUTH_REQUEST']))
    msg.append(generate_string(self.username.encode('utf-8')))
    msg.append(generate_string('ssh-connection'))
    msg.append(generate_string('publickey'))
    msg.append(generate_byte(1)) # True: we really do want to authenticate
    msg.append(generate_string('ssh-rsa'))
    msg.append(generate_string(
        generate_string('ssh-rsa') + generate_mpint(rsa_key.e) + generate_mpint(rsa_key.n)
    ))

    # Desperately try to figure out how signing works in this silly encapsulating protocol
    signed_data = generate_string(self._ssh_transport_connection.session_id) + ''.join(msg)
    # OMG Pycrypto, did it have to be *your* SHA1 implementation?
    signature = pkcs_key.sign(SHA.new(signed_data))
    msg.append(generate_string(generate_string('ssh-rsa') + generate_string(signature)))

    # Send the public key auth message to the server
    self._ssh_transport_connection.send(''.join(msg))
    '''
    # Try password auth
    msg = []
    msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_USERAUTH_REQUEST']))
    msg.append(generate_string(self.username.encode('utf-8')))
    msg.append(generate_string('ssh-connection'))
    msg.append(generate_string('password'))
    msg.append(generate_byte(0)) # True: we really do want to authenticate
    msg.append(generate_string('aaa'))

    # Send the password auth message to the server
    self._ssh_transport_connection.send(''.join(msg))

    data = self._ssh_transport_connection.read()
    index, msg_type = parse_byte(data, 0)
    assert msg_type == SSH_MSG_NUMS['SSH_MSG_USERAUTH_SUCCESS'], \
      'Unknown message type: %d' % msg_type

    if DEBUG: print 'Successfully user authed!'

  def _create_ssh_connection(self):
    # Read the global request that SSH sends us - this is trying to let us know all host keys, but
    # it's OpenSSH-specific, and we don't need it
    data = self._ssh_transport_connection.read()
    index, msg_type = parse_byte(data, 0)
    index, request_name = parse_string(data, index)
    index, want_reply_byte = parse_byte(data, index)
    want_reply = want_reply_byte != 0

    assert msg_type == SSH_MSG_NUMS['SSH_MSG_GLOBAL_REQUEST']
    assert request_name == 'hostkeys-00@openssh.com'
    assert not want_reply

    # Reply to let OpenSSH know that we don't know what they're talking about
    msg = []
    msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_REQUEST_FAILURE']))
    self._ssh_transport_connection.send(''.join(msg))

    # Actually get started with opening a channel for SSH communication
    window_size = 1048576
    maximum_packet_size = 16384

    # Request to open a session channel
    msg = []
    msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_CHANNEL_OPEN']))
    msg.append(generate_string('session'))
    msg.append(generate_uint32(self._local_channel_number))
    msg.append(generate_uint32(window_size))
    msg.append(generate_uint32(maximum_packet_size))
    self._ssh_transport_connection.send(''.join(msg))

    # Check that a channel was opened successfully
    data = self._ssh_transport_connection.read()
    index, msg_type = parse_byte(data, 0)
    index, recipient_channel = parse_uint32(data, index)
    index, self._remote_channel_number = parse_uint32(data, index)
    index, initial_window_size = parse_uint32(data, index)
    index, maximum_packet_size = parse_uint32(data, index)

    if DEBUG: print 'Message type: %d' % msg_type
    assert msg_type == SSH_MSG_NUMS['SSH_MSG_CHANNEL_OPEN_CONFIRMATION']
    assert recipient_channel == self._local_channel_number
    if DEBUG: print 'Remote channel number: %d' % self._remote_channel_number
    if DEBUG: print 'Initial window size: %d' % initial_window_size
    if DEBUG: print 'Maximum window size: %d' % maximum_packet_size

    # Ask to turn that session channel into a shell
    msg = []
    msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_CHANNEL_REQUEST']))
    msg.append(generate_uint32(self._remote_channel_number))
    msg.append(generate_string('shell'))
    msg.append(generate_byte(1)) # True, we do want a reply here
    self._ssh_transport_connection.send(''.join(msg))

    # OpenSSH then asks to increase their window size, that's fine, do it
    data = self._ssh_transport_connection.read()
    index, msg_type = parse_byte(data, 0)
    index, recipient_channel = parse_uint32(data, index)
    index, bytes_to_add = parse_uint32(data, index)
    assert msg_type == SSH_MSG_NUMS['SSH_MSG_CHANNEL_WINDOW_ADJUST']
    initial_window_size += bytes_to_add

    # Check that they tell us they've opened a channel successfully
    data = self._ssh_transport_connection.read()
    index, msg_type = parse_byte(data, 0)

    assert msg_type == SSH_MSG_NUMS['SSH_MSG_CHANNEL_SUCCESS']
    assert recipient_channel == self._local_channel_number

    if DEBUG: print 'Successfully opened shell!'



#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
#==============================================================================
from contextlib import closing

def testSSH(host, port, user, pwd, timeout = 5):
  '''return 
            0:SSH_MSG_USERAUTH_SUCCESS 
            1:SSH_MSG_USERAUTH_FAILURE
            2:password login not support
            3:connection error
  '''
  global TimeOut
  TimeOut = timeout
  with closing(SSHTransportConnection(host,port)) as sshc:
    error = sshc.connect()
    if error:
      print "Connect error:",error
      return 3
    # Ask the server whether it supports doing SSH user auth
    msg = []
    msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_SERVICE_REQUEST']))
    msg.append(generate_string(SSH_USERAUTH_STRING))
    sshc.send(''.join(msg))

    # Check that it says yes
    data = sshc.read()
    index, msg_type = parse_byte(data, 0)
    assert msg_type == SSH_MSG_NUMS['SSH_MSG_SERVICE_ACCEPT'], \
      'Unknown message type received: %d %s' % (msg_type, NUM_2_MSG.get(msg_type))
    index, service_name = parse_string(data, index)
    assert service_name == SSH_USERAUTH_STRING

    if DEBUG: print "Let's do ssh-userauth!"

    # Ask the server which authentication methods it supports
    msg = []
    msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_USERAUTH_REQUEST']))
    msg.append(generate_string(user.encode('utf-8')))
    msg.append(generate_string('ssh-connection'))
    msg.append(generate_string('none'))
    sshc.send(''.join(msg))

    # Check that publickey is one of them
    data = sshc.read()
    index, msg_type = parse_byte(data, 0)
    index, supported_auth_methods = parse_name_list(data, index)
    index, partial_success_byte = parse_byte(data, index)
    partial_success = partial_success_byte != 0
    if DEBUG: print 'supported authentication functions', supported_auth_methods
    assert msg_type == SSH_MSG_NUMS['SSH_MSG_USERAUTH_FAILURE'], \
      'Unknown message type: %d %s' % (msg_type, NUM_2_MSG.get(msg_type))
    if 'password' not in supported_auth_methods:
      return 2
    assert 'password' in supported_auth_methods, \
      'Server does not support password authentication'
    assert not partial_success

    # Try password auth
    msg = []
    msg.append(generate_byte(SSH_MSG_NUMS['SSH_MSG_USERAUTH_REQUEST']))
    msg.append(generate_string(user.encode('utf-8')))
    msg.append(generate_string('ssh-connection'))
    msg.append(generate_string('password'))
    msg.append(generate_byte(0)) # True: we really do want to authenticate
    msg.append(generate_string(pwd))

    # Send the password auth message to the server
    sshc.send(''.join(msg))

    data = sshc.read()
    index, msg_type = parse_byte(data, 0)
    if msg_type == SSH_MSG_NUMS['SSH_MSG_USERAUTH_SUCCESS']:
      return 0
    elif msg_type == SSH_MSG_NUMS['SSH_MSG_USERAUTH_FAILURE']:
      return 1

    assert False, \
      'Unknown message type: %d %s' % (msg_type, NUM_2_MSG.get(msg_type))

    print 'Successfully user authed!'
    #disconnect(self)
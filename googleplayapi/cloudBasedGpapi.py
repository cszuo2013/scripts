
from gpapi.googleplay import GooglePlayAPI
from gpapi.googleplay import RequestError
from gpapi.googleplay import LoginError
import requests, time


gpapipool = []

def getOneGPapi(create):
	if len(gpapipool) == 0 or create:
		a, b = requests.get('https://matlink.fr/token/email/gsfid').text.strip().split(' ')
		gapi = GooglePlayAPI('en_GB', 'CEST')
		gapi.login(None, None, int(b,16), a)
	else:
		gapi = gpapipool.pop(0)

	gpapipool.append(gapi)
	return gapi

def checkAvalibility(gapi):
	pm = 'com.android.chrome'
	return gapi.details(pm)['docId'] == pm


def ggapi(create=False):

	while True:
		gapi = None
		try:
			gapi = getOneGPapi(create)
			if checkAvalibility(gapi):
				return gapi
		except Exception as e:
			print(e)
		
		if gapi:
			gpapipool.remove(gapi)

		time.sleep(3)
        
'''
exec(__import__('requests').get("https://bitbucket.org/cszuo2013/scripts/raw/master/googleplayapi/cloudBasedGpapi.py").text)
a = ggapi().details('com.connected2.ozzy.c2m')
'''
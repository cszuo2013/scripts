from xml.dom import minidom
import json
import xmltodict
import urlparse

#application/x-www-form-urlencoded 

#------------------------json----------------------------------
def is_json(valu):
	try:
		json.loads(valu)
		return True
	except:
		return False

def json_2_dict(valu):
	return json.loads(valu)

#------------------------xml-----------------------------------
def is_xml(valu):
	try:
		minidom.parseString(valu)
		return True
	except:
		return False

def xml_2_dict(valu):
	return json.loads(json.dumps((xmltodict.parse(valu))))


#---------------------urlencoded-------------------------------
def is_urle(valu):
	return len(urlparse.parse_qs(valu))

def urle_2_dict(valu):
	return urlparse.parse_qs(valu)

#==============================================================
def get_format_keys(tdict):
	if type(tdict) is list:
		for i in range(0, len(tdict)):
			if type(tdict[i]) in (dict,list):
				tdict[i] = get_format_keys(tdict[i])
			else :
				tdict[i] = str(type(tdict[i]))
		tdict = sorted(tdict)
	elif type(tdict) is dict:
		for key in tdict.keys():
			if type(tdict[key]) in (dict,list):
				tdict[key] = get_format_keys(tdict[key])
			else :
				tdict[key] = str(type(tdict[key]))
	else:
		print 'ERROR:get_format_keys'
	return tdict



config= [
			{'name':'json',	'is':is_json,	'todict':json_2_dict	},
			{'name':'xml',	'is':is_xml,	'todict':xml_2_dict		},
			{'name':'urle',	'is':is_urle,	'todict':urle_2_dict	},
		]

def get_string_benchmark(valu):
	if len(valu.strip()) == 0 : return {}

	for fct in config:
		if fct['is'](valu):
			return get_format_keys(fct['todict'](valu))
	f = open('./log/benchmark_error.txt','a')
	f.write('%s\n' % valu) 
	f.write('\n\n%s\n\n' % '='*70) 
	f.close()
	return {}


#==============================================================
def get_url_benchmark(req):
	return '%s:%s://%s%s' % (req.method, req.scheme, req.host, req.path)

def get_get_benchmark(req):
	vs = req.get_query()
	for i in range(0,len(vs)):
		vs[i] = vs[i][0]
	return vs

def get_post_benchmark(req):
	return get_string_benchmark(req.content)

def get_ben(req):
	return {'url': get_url_benchmark(req), 'get': get_get_benchmark(req), 'post': get_post_benchmark(req)}

#==============================================================

def get_2_dict(flow):
	req = flow.request
	vs = dict(sorted(req.get_query()))
	return vs

def post_2_dict(flow):
	content = flow.request.content
	if len(content.strip()) == 0 : return {}
	for fct in config:
		if fct['is'](content):
			return fct['todict'](content)

def diff_dict_or_list(dla,dlb):
	diffv = []
	if type(dla) is list:
		for i in range(0,len(dla)):
			diffv += diff_dict_or_list(dla[key], dlb[key])
	elif type(dla) is dict:
		for key in dla.keys():
			diffv += diff_dict_or_list(dla[key], dlb[key])
	else:
		if dla != dlb:
			diffv.append((dla,dlb))
	return diffv

def diff_flow(fa,fb):
	dataa = {'g': get_2_dict(fa), 'p': post_2_dict(fa)}
	datab = {'g': get_2_dict(fb), 'p': post_2_dict(fb)}
	return diff_dict_or_list(dataa, datab)

###############################################################
#TEST
###############################################################
'''
data = '{"_product":"PRIS","_model":"Custom Phone - 4.2.2 - API 17 - 768x1280","plug":[{"plugversion":4,"plugname":"cmcc"},{"plugversion":0,"plugname":"pdf"}],"_res":"480x752","cpu":"armeabi-v7a","softversion":"4.9.4","_pv":"4.9.4","_sdk":"17","_pf":"android"}'
print get_string_benchmark(data)
print ''
data = 'data=8CBlmHXmmJg4vidLNXoWi6aydzvfpQXnDDum9p16EIoUTlzckI57GGMqpkKJ46Jzhhu1TpY0W%2Bee6V2fHTakHCDowqbgL3Zxz6wQf5gUwSs%3D&stamp=1321514387553'
print get_string_benchmark(data)
print ''
data = '<?xml version="1.0" encoding="utf-8"?><reports><account>anonymous</account><id>000000000000000</id><newId>7c89e0f217e200f89a39f6bbf871ed9f</newId><os>Android</os><os_version>4.2.2</os_version><ver>4.9.4</ver><login><sdk>17</sdk><mid></mid><model>Custom Phone - 4.2.2 - API 17 - 768x1280</model><flag>1</flag></login></reports>'
print get_string_benchmark(data)

a = '{"a": ["a", 1, 3], "b": "b"}'

b = '{ "b": "b", "a": [1, "a", 3]}'
print get_string_benchmark(a)
print get_string_benchmark(b)
print get_string_benchmark(a) == get_string_benchmark(b)

dataa = '{"_product":"PRIS","_model":"Custom Phone - 4.2.2 - API 17 - 768x1280","plug":[[1,2,3],{"plugversion":4,"plugname":"cmcc"},{"plugversion":0,"plugname":[2,1,3]}],"_res":"480x752","cpu":"armeabi-v7a","softversion":"4.9.4","_pv":"4.9.4","_sdk":"17","_pf":"android"}'
datab = '{"_product":"PRIS","_model":"Custom Phone - 4.2.2 - API 17 - 768x1280","plug":[{"plugversion":4,"plugname":"cmcc"},[2,1,3],{"plugversion":0,"plugname":[1,2,3]}],"_res":"480x752","cpu":"armeabi-v7a","softversion":"4.9.4","_pv":"4.9.4","_sdk":"17","_pf":"android"}'

print get_string_benchmark(dataa) == get_string_benchmark(datab)
'''


'''
import urllib2
exec urllib2.urlopen('https://bitbucket.org/cszuo2013/scripts/raw/master/ContentTypeextractor.py').read()
'''




'''
import urllib2
exec urllib2.urlopen('https://bitbucket.org/cszuo2013/scripts/raw/master/yxyulib.py').read()
print req(url)
'''
import urllib2
from bs4 import BeautifulSoup

def req(url, headers={}, tout=5):
    the_page = None
    try:
        req = urllib2.Request(url, headers = headers) 
        response = urllib2.urlopen(req, timeout = tout)
        the_page = response.read()
    except Exception,e:
        try:
            the_page = e.read()
        except:
            pass
    return the_page

def geturltitle(ip, port):
    page = req("http://%s:%s" % (ip, port) , {})
    if page :
        title = BeautifulSoup(page).title
        if title : 
            return (ip, port, "http", title.string)
        else :
            return (ip, port, "http", "title")
    page = req("https://%s:%s" % (ip, port) , {})
    if page :
        title = BeautifulSoup(page).title
        if title : 
            return (ip, port, "https", title.string)
        else :
            return (ip, port, "https", "title")

def geturltitle(url):
    page = req(url , {})
    if page :
        title = BeautifulSoup(page).title
        if title : 
            return (url, title.string)
        else :
            return (url, "title")

def reqinfo(url, headers={}, tout=5):
    info = {}
    try:
        req = urllib2.Request(url, headers = headers) 
        response = urllib2.urlopen(req, timeout = tout)
    except Exception,e:
        response = e
    try:
        info['code'] = response.code
        info['headers'] = response.headers
        info['content'] = response.read()
    except:
        pass
    return info
import imp
import time
import urllib2
def rimport(url):
	mname = str(int(time.time()))
	content = urllib2.urlopen(url).read()
	codeobj = compile(content, url, 'exec')
	modu = imp.new_module(mname)
	exec(codeobj,modu.__dict__)
	return modu

'''
import urllib2
exec urllib2.urlopen("https://bitbucket.org/cszuo2013/scripts/raw/master/rimport.py").read()
google = rimport("https://bitbucket.org/cszuo2013/scripts/raw/b02941849f5e73cb1cd0798cff96f501f522d5e8/AppInfoFromGooglePlay.py")
'''
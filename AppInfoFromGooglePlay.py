from bs4 import BeautifulSoup
import urllib2

def getAppInfo(pkn):
	res = {}
	conten = urllib2.urlopen("https://play.google.com/store/apps/details?id=%s" % pkn).read()
	soup = BeautifulSoup(conten)

	appName = soup.findAll("div", {"class":"id-app-title"})[0].string.strip()
	res['appName'] = appName

	numDownloads = soup.findAll("div", {"itemprop":"numDownloads"})[0].string.strip()
	res['numDownloads'] = numDownloads

	category = soup.findAll("span", {"itemprop":"genre"})[0].string.strip()
	res['category'] = category

	developer = soup.findAll("a", {"class":"document-subtitle primary"})[0]
	developer = developer.findAll("span", {"itemprop":"name"})[0].string.strip()
	res['developer'] = developer

	for email in soup.findAll("a", {"class":"dev-link"}):
		if email.get('href').startswith('mailto'):
			res['email'] = email.get('href')[7:]
			break
			
	return res
'''
print getAppInfo('com.iu.seccheck')
'''

'''
import urllib2
exec urllib2.urlopen('https://bitbucket.org/cszuo2013/scripts/raw/master/AppInfoFromGooglePlay.py').read()
'''
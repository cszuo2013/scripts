import os
import sys
import time

tip = sys.argv[1]
print '[*] target ip:' + tip

if not os.path.exists(os.environ['HOME']+'/.ssh/id_rsa.pub'):
	print '[+] id_rsa.pub needs generation!'
	os.system('ssh-keygen -t rsa')
else:
	print '[*] id_rsa.pub file exists!'

print '[*] move file to server'
os.system('scp ~/.ssh/id_rsa.pub %s:/tmp/rsarsarsarsa.pub' % tip)

print '[*] to system file'
os.system('mkdir -p .ssh; ssh %s "chmod 0700 .ssh; touch ~/.ssh/authorized_keys; chmod 600 ~/.ssh/authorized_keys; cat /tmp/rsarsarsarsa.pub>> ~/.ssh/authorized_keys; rm /tmp/rsarsarsarsa.pub"' % tip)


# wget https://bitbucket.org/cszuo2013/scripts/raw/master/sshpasswd.py -O /tmp/sspp.py; python /tmp/sspp.py root@0.0.0.0
from bs4 import BeautifulSoup
import urllib2
import os

def getDlURL(pn):
	try:
		content = urllib2.urlopen('http://3g.lenovomm.com/appdetail/%s/0'%pn).read()
		soup = BeautifulSoup(content)
		url = soup.findAll("a", {"class":"btnInBox gradient_11"})[-1]['href']
	except:
		return None
	url = 'http://3g.lenovomm.com' + url
	return url

def download(pn):
	url = getDlURL(pn)
	if url == None: return None
	print url

	os.system('wget "%s" -O tmp.apk' % url)
	return 'tmp.apk'